{
    "id": "bbd40dbd-0c78-41f1-9e65-a463e34e3337",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_battlemenu",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Courier New",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "65d1d7e0-1838-4e85-a136-7d46869a2221",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ebeb4d4a-7aa8-4ba5-9e54-c5762b3be7ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 12,
                "offset": 3,
                "shift": 7,
                "w": 1,
                "x": 85,
                "y": 44
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "41a432cb-642a-4b8b-b0ee-8d2ee935bcc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 79,
                "y": 44
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "8970b531-af31-4114-b557-99c639abad7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 72,
                "y": 44
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "d7e79486-68c8-472e-9796-17af4ff879ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 66,
                "y": 44
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "f257ab1c-d98f-4d6d-9f2e-2a9a0a14e462",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 59,
                "y": 44
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "452291ac-656e-41e5-8da8-ceb724452f59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 53,
                "y": 44
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "dc13d730-efae-4da5-9690-cbc047c3f565",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 12,
                "offset": 3,
                "shift": 7,
                "w": 1,
                "x": 50,
                "y": 44
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "15463d23-2c9b-4f22-9dd6-73f18397be57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 12,
                "offset": 3,
                "shift": 7,
                "w": 2,
                "x": 46,
                "y": 44
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "235a99ef-8d1c-453c-8246-07b48d22fc4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 12,
                "offset": 2,
                "shift": 7,
                "w": 1,
                "x": 43,
                "y": 44
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "1f043a3b-6307-4605-9d0e-6c381b8955cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 88,
                "y": 44
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "a7cbdd1e-1d9b-49c1-a81f-e2582c19ae87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 36,
                "y": 44
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "1b79a7c7-d561-4810-9d58-0b7389186e93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 12,
                "offset": 2,
                "shift": 7,
                "w": 2,
                "x": 25,
                "y": 44
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "49827f94-73e9-4b86-b5ae-18c45a801c2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 18,
                "y": 44
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "d12a630d-abe6-46b8-9473-bbbea5886702",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 12,
                "offset": 2,
                "shift": 7,
                "w": 2,
                "x": 14,
                "y": 44
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "d1233532-e523-435d-b875-39b2fd4d15a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 8,
                "y": 44
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "9f77abab-909d-49c1-98b3-3ef89ba673d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "1972fa06-0a3f-435c-9cbf-53ca13abf29a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 120,
                "y": 30
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "c4b1ded5-4a88-4286-8691-3c130f682e47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 114,
                "y": 30
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "e468a9be-ea55-4020-9816-b791e30b6028",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 107,
                "y": 30
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "bfa1d001-0c4f-448c-9f72-21894896493d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 101,
                "y": 30
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "5b3ed980-5728-48ed-9940-34f875a2d9f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 29,
                "y": 44
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "82297f12-7b3f-4efc-8830-a0ede5c677ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 94,
                "y": 44
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "460c3cdc-826c-4dd4-ab69-3dcf7a0ed495",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 101,
                "y": 44
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "99ef610b-7a95-4348-84bd-affdb927816b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 107,
                "y": 44
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "a277ebf0-e729-4718-acd7-d26a62f438ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 6,
                "y": 72
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "063d8881-3955-492e-95e5-ba9159161112",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 12,
                "offset": 2,
                "shift": 7,
                "w": 2,
                "x": 2,
                "y": 72
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "82da53dc-ea08-4c1b-ae73-c55886eb0212",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 12,
                "offset": 2,
                "shift": 7,
                "w": 2,
                "x": 123,
                "y": 58
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "9cce9a58-5818-4caf-8242-2592e0d5afd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 116,
                "y": 58
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "35c996b7-c9a7-41da-9f27-208d3bc5a685",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 109,
                "y": 58
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "eca2627a-2fb6-4063-9e74-fff527afb382",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 102,
                "y": 58
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "82c8397e-d1d1-4d1f-87f2-2f0c34c6b093",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 96,
                "y": 58
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "f755782a-b244-416a-b545-a57bec115540",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 90,
                "y": 58
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "dd066502-8c9e-487f-bc77-86c8460035f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 81,
                "y": 58
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "5635e20e-40b9-4705-8969-daf6186c14cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 73,
                "y": 58
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "01d8d66a-9c04-4fe6-b1bb-67953379ae02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 66,
                "y": 58
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "ba9f7396-9e81-4034-b6eb-4d0b7503e5cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 59,
                "y": 58
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "8bfaf182-16fe-47f7-8a00-72c8524b2a60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 51,
                "y": 58
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "019154ea-7d33-4aeb-bfcf-46194852f82f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 44,
                "y": 58
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "de2f92f9-c8ba-4d03-8614-9ee12f26ad48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 37,
                "y": 58
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "3f17c7cf-c1b5-42f0-982a-fc765935fffe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 30,
                "y": 58
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "862cb9ea-4239-4d41-bd62-b8eca7ac3731",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 24,
                "y": 58
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "2b19fe1b-aa2a-4fa5-bccf-80d868fafe19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 17,
                "y": 58
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "9e4f744a-20be-42a2-a63b-a83ec928a185",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 9,
                "y": 58
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "d76ad126-b863-47da-8b62-74c7d8e16db3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 58
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "49ce3a0e-a3d0-4645-970f-c734c41decd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 113,
                "y": 44
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "ba6f0210-1754-455c-a98d-a1055e25fed4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 93,
                "y": 30
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "377f5156-11d2-4a4b-ac9d-56187d95129b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 86,
                "y": 30
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "09666e2f-9399-44ca-84ba-187f9c1d79c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 79,
                "y": 30
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d042fb99-23ac-4c3d-9595-21a37bbc366a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 37,
                "y": 16
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "4e44f0ed-6aec-4b12-b014-bca589811f8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 24,
                "y": 16
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "3c743682-dacb-4134-bcac-be5e9211ba13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 17,
                "y": 16
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "e59fabbd-edaa-4981-ae6c-9fa2f8409a04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 10,
                "y": 16
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "3fa2b1d5-0f2d-4483-a2bc-6cb62d1c79dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 16
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "f7d2fe6c-6379-45d4-9e58-e408a0ffeb8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "1cdcaa39-2353-40e1-82a5-34fbf2124b17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "2f3aad18-4aca-4d46-b259-d516625e38c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "f3db03a6-88ea-42c9-8e64-3054e8ca0d9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "cae5d91b-ad93-45f9-a391-2182ea130600",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "22a50855-fb13-44f7-b0ce-898310610613",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 12,
                "offset": 3,
                "shift": 7,
                "w": 2,
                "x": 33,
                "y": 16
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "500c27e5-7e0b-443c-94e3-6ca696b74ed4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "858c658e-1a2b-4927-9333-77ccaab90d17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 12,
                "offset": 2,
                "shift": 7,
                "w": 2,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "2619accf-0df6-47e8-910d-f1009b2892bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "1c7a529e-f5ab-4547-b6be-f48ed4b01fe4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "97ed7388-2b1b-475a-9aaf-a58add4d3e7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 12,
                "offset": 2,
                "shift": 7,
                "w": 2,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "e7437468-4c89-4cba-b210-1251f1aeb4c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "8e1c9e46-161a-41ba-973c-5fd6d2c0321d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "cbca5008-23c0-48a9-a370-39717152bd2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 25,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "ec631755-faf7-4089-809a-0798707816a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "c94c505a-555e-42df-ad73-7b629d2036b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "0d271cbf-abd6-47c6-9298-20c3938eceae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 70,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "7552d78c-bb06-439d-bda5-0a6279747abd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 44,
                "y": 16
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "169a443e-dd5c-4f6a-a640-0fe2171ff8eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 113,
                "y": 16
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "988cb94d-b56d-4a73-b114-8ab4df05f9a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 51,
                "y": 16
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "524cf015-8944-4fea-a937-780f5d80c237",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 3,
                "x": 68,
                "y": 30
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "be7743e0-e53b-4176-bd63-9f942412d7e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 61,
                "y": 30
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "20a7b874-6649-4226-a4eb-703413018acc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 54,
                "y": 30
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "26db358b-ad65-4de3-8a3c-6aecc8dabcca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 45,
                "y": 30
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "23ac4687-359c-4f87-9798-2680f6583bb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 38,
                "y": 30
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d4f784d4-3e8b-4243-8599-daf59de6111d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 31,
                "y": 30
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "2076a831-3fb7-4811-80d6-2b7fe52cb78a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 23,
                "y": 30
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "1381692b-2102-4843-a34e-b01c240f46f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 16,
                "y": 30
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "f476ac83-755f-4d4b-b80b-37fbc844ef98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 9,
                "y": 30
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "46ba5fd9-c410-48eb-9fbd-15041b75aeab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 73,
                "y": 30
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "f0294954-af1f-40d9-9936-9882009a55f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 30
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "53f8ac15-261f-42f6-a967-091137a1fc4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 105,
                "y": 16
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "7325f766-623f-49e1-acf6-f688f8ab69fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 97,
                "y": 16
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "34ef000b-0b96-4125-87bf-9641565e86c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 89,
                "y": 16
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "74a8f381-b751-4ffe-86aa-2edb10c9dcee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 82,
                "y": 16
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "ae6c761e-5bfb-43bb-86b9-87c146576f8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 75,
                "y": 16
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "3ae9ff0a-89c8-467b-9308-fa1ad50f9693",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 69,
                "y": 16
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "10000123-5c24-479f-a032-63375f9f9397",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 12,
                "offset": 2,
                "shift": 7,
                "w": 2,
                "x": 65,
                "y": 16
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "97175961-240d-4b3e-805b-3a951a02315f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 12,
                "offset": 3,
                "shift": 7,
                "w": 1,
                "x": 62,
                "y": 16
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "a6888758-70df-4e4b-8f0a-fed9d27c931d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 12,
                "offset": 2,
                "shift": 7,
                "w": 2,
                "x": 58,
                "y": 16
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "fd89d9be-8214-419e-acf6-aa8281e85330",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 13,
                "y": 72
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "998a1cbd-5689-451b-8fbc-be779b19251e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 12,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 20,
                "y": 72
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 8,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}
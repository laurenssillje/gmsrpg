{
    "id": "bbd40dbd-0c78-41f1-9e65-a463e34e3337",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_battlemenu",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Courier New",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "0b6cc640-74b3-483d-ba8c-ea756aa250dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "a5ccb6df-5949-4739-a723-7b43d64af208",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 15,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 16,
                "y": 70
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "ecedca1b-d84b-438c-9932-c5ba68f0d4ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 15,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 10,
                "y": 70
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "4abde2ba-7421-4042-96b8-5bb5c852673a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 70
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "de35f201-683c-4efb-ad2b-e45864ad1f56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 113,
                "y": 53
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "98b2ace3-1d63-43ad-bc5d-0197d666d06d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 105,
                "y": 53
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "e47c7009-815e-47b1-84d1-8deb27d3c764",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 15,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 99,
                "y": 53
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "4297c87d-0510-431e-b114-7e3692b4e1b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 15,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 95,
                "y": 53
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "4c53dc80-a764-45ae-9423-ce7644e4b49e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 15,
                "offset": 4,
                "shift": 8,
                "w": 2,
                "x": 91,
                "y": 53
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "36947679-5e11-4bdd-ab8f-03992cd532a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 15,
                "offset": 2,
                "shift": 8,
                "w": 2,
                "x": 87,
                "y": 53
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "5448881d-4609-4c9a-b4c0-64f2480c67bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 20,
                "y": 70
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "b5c1376d-e9a7-4fd2-98f5-d7385e05b11e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 79,
                "y": 53
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "3c9e3b08-8486-4ad8-a960-c815e16e346d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 15,
                "offset": 2,
                "shift": 8,
                "w": 3,
                "x": 66,
                "y": 53
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "34a53241-59d6-47b7-8b6f-58c568657839",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 58,
                "y": 53
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "3f9edb88-bbf8-405a-8774-93a141ad30be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 15,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 54,
                "y": 53
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "401785af-0a8e-4efa-bfbc-f9d8525048b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 47,
                "y": 53
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "edf76947-0d6a-497e-b02b-b7409ba389a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 40,
                "y": 53
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "be9256c7-0358-4f96-92a1-3e9e37f4d07b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 33,
                "y": 53
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "94c89e17-6c57-43e7-83ef-e6b6afa89e94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 26,
                "y": 53
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "232cbca4-b338-4cc9-b818-d2a5d5285eed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 18,
                "y": 53
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "537dbccd-50c3-4aed-b14a-f485e5fdb070",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 11,
                "y": 53
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "ade14c02-b742-4ae7-8b60-5a0f57761ccc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 71,
                "y": 53
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "64ac8820-a10a-459b-8970-ecc37e1dae11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 15,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 27,
                "y": 70
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "0636c6a9-5d9f-40d7-bad8-b5994c936613",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 34,
                "y": 70
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "442b0d3a-a897-435d-af37-e6fbcc87fe79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 41,
                "y": 70
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "cef1e89f-4fee-4a52-91bc-44bd3613a7af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 15,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 82,
                "y": 87
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "659cf684-c429-4a37-8c5b-34209c74c303",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 15,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 78,
                "y": 87
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "62c2d9eb-44e4-4d81-997c-55a3f0229e49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 15,
                "offset": 2,
                "shift": 8,
                "w": 3,
                "x": 73,
                "y": 87
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "871ae17f-0d57-4db0-ac3f-c9c8abef51c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 65,
                "y": 87
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "e9577d25-47ba-40b4-bf11-d7cc6689a314",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 57,
                "y": 87
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "fdd8ec0a-9967-4eb5-ad46-c235b47d923d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 49,
                "y": 87
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "8ea76469-531c-4692-97a8-023e22c19686",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 15,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 43,
                "y": 87
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "5b189767-ed62-4981-b813-f137d8357796",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 36,
                "y": 87
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "86559b19-f645-4237-8d8c-faed407e34af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 26,
                "y": 87
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "381595e3-55db-4cf7-b21d-51cf9cabc51c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 18,
                "y": 87
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "67406190-4acb-4114-a545-95be2755bd6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 10,
                "y": 87
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "399a98cf-6c17-4b53-bd0c-edc48d672144",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 87
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "9be2fae1-821e-4a96-bf42-0e16b52084c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 114,
                "y": 70
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "05707fb0-fe3c-4a88-8955-555154996b7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 106,
                "y": 70
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "2eb3629f-f86b-4b42-8746-9f32b89ab76a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 98,
                "y": 70
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "dad39a5e-bc26-484c-8c68-1a812d0d5e6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 90,
                "y": 70
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d83ee14c-e33f-486d-92ba-a77050775b0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 83,
                "y": 70
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "910d0ed2-fe9a-40bb-81e6-6ef1c3a274d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 74,
                "y": 70
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "dd8e9d9b-82ed-433b-8fa8-f2074a363c50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 66,
                "y": 70
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "fd7f0e0f-bdd8-4846-b529-fb44e633ab92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 58,
                "y": 70
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "9b82e0e9-7940-4f4c-8839-4aede9558f9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 48,
                "y": 70
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "dab3bbf1-4439-4790-8600-d44eede14286",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 53
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "60a2a3af-4c29-4b0b-982a-c3af8d8a581e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 115,
                "y": 36
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "8f79cea1-54b0-44cf-a1e6-14b0e208625a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 107,
                "y": 36
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a779fc48-1bdf-49ce-847d-ad12c2e4e62f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 49,
                "y": 19
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "87d41cb9-f53f-44d4-bf8b-cc8ac48d3447",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 36,
                "y": 19
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "14c57f22-40d6-4fa5-8a6f-f764b103528c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 28,
                "y": 19
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "704c0da3-d235-4846-95b9-52b1eed628b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 20,
                "y": 19
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "536e589f-7121-4fab-ada5-0fa70f6dd8b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 12,
                "y": 19
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "b5dcf770-bb0a-44c4-85a6-e74404e58705",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 19
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "bff31ad0-31b8-4040-bb76-ff0db439cd31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "c459684a-cc92-4a91-be33-89e1bdb9e4c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "72885833-d3e6-4852-969d-071bb47a321a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "3c356501-5856-4a0f-99fa-ed8d06f14eea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "9d916395-0f5a-4113-bcf4-57d7a7797e61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 15,
                "offset": 4,
                "shift": 8,
                "w": 2,
                "x": 45,
                "y": 19
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "3168cb16-776f-4545-bf77-67355e3e92c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "95ad4375-d6c0-4bb2-bc37-b180d854ed49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 15,
                "offset": 2,
                "shift": 8,
                "w": 2,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "834a88e2-afdf-4adf-a7fb-e61e79277cb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "d159bb65-73da-4e98-b233-648a579b7239",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "80ef4d58-6927-4c6f-8885-235732051262",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 15,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "c14c8632-5053-4735-b654-1a47aaec37a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "6069577c-3d29-4e3c-9d30-c8eb3c4260b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "f17d28f6-5000-4a30-b9cc-d3f809efa508",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 29,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "ddc39384-784d-4bee-87cc-845e54e35108",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "773d2ee1-8c56-4ee6-80f2-c50ccba02432",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "fab2e45a-8aa1-40f5-9758-84657f826e83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "e8396028-9419-4538-8663-2f39bf07b8f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 57,
                "y": 19
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "36237500-4347-4332-935f-de8741c9af16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 10,
                "y": 36
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "7d610484-6edb-4408-8a04-1e0fc2222881",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 65,
                "y": 19
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "68196262-2d82-4a75-8b99-d016f5b6a857",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 4,
                "x": 94,
                "y": 36
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "363a8f74-6681-4fb0-af1b-691b705cdc83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 86,
                "y": 36
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "b2e9ad14-2577-4ff2-841d-1178d38cd5da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 78,
                "y": 36
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "59f4d4a4-f594-4aa5-91b7-820c25fc96f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 68,
                "y": 36
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "483156dd-a69d-45bd-8355-36ca47c291f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 60,
                "y": 36
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "df4008c5-bfe9-4cf8-a124-c849006bf54d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 52,
                "y": 36
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "d770ec41-2a49-4c87-9182-6286f09cbafb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 43,
                "y": 36
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "43a8964d-36a2-42ce-b96d-9df2484cf2be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 34,
                "y": 36
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "a69e631a-9b61-456c-be91-8377b783e2c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 26,
                "y": 36
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "659593c9-65e1-4b2a-82c3-c6fbee165e4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 100,
                "y": 36
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "6409cf25-1554-450d-b259-110dceb858f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 18,
                "y": 36
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "1c8feda6-c2ee-499b-b23d-55c66c7ea935",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 36
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "ea9ed273-2b39-4fac-aea3-7187c15eff58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 116,
                "y": 19
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "91c6cfbe-0fe2-49fe-be5d-01dbc5a7ada6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 107,
                "y": 19
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "cbe6c130-f3f5-4188-afcb-791781d9d2c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 99,
                "y": 19
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "58fa5482-c0cf-431c-8c92-8ddc01a4148d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 91,
                "y": 19
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "6cb87d6b-0209-454c-9e6a-a8b7651fe7e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 15,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 85,
                "y": 19
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "272d7749-cc14-4f2f-aa46-bb2cd9b2603e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 15,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 81,
                "y": 19
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "d11e1dbc-b8b4-4a72-a273-cdc0ed58ddc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 15,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 77,
                "y": 19
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "4ec8c862-c708-4052-98b8-0d44e2a049da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 15,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 73,
                "y": 19
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "76b39f53-b7b8-4a6d-910f-ee191e756d8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 89,
                "y": 87
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "8519cc51-7cbd-4c92-8345-83b0ef3a408e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 15,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 97,
                "y": 87
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 10,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}
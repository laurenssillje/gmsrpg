{
    "id": "d1665cd2-e4a8-42ea-9cc8-1fd2a9b40e5d",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_dialogue",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Courier New",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "eb75e46a-8bbb-4d7c-89cd-a825f8185f28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "cf4085d2-4816-4bf0-b78f-cb790baab9db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 18,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 89,
                "y": 42
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "84c1ab6a-073e-46cb-84e6-ac1236f72191",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 81,
                "y": 42
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "29edbaf3-763a-4d9f-a0ba-781dc1f01781",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 72,
                "y": 42
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "508d35d7-b0f5-4f8a-9f86-7838ea2f353d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 64,
                "y": 42
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "d1123f9f-d883-4ffc-8157-f1867084fa37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 55,
                "y": 42
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "7c5fb12a-4f8f-4018-96c6-34bb9813236a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 47,
                "y": 42
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "a8382549-e255-44b8-8561-d966cc7b58d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 18,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 43,
                "y": 42
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "61d9b929-2dee-47e8-b359-88e0090b1994",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 18,
                "offset": 5,
                "shift": 10,
                "w": 2,
                "x": 39,
                "y": 42
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "7f805906-ea74-4ef3-b4d6-ba61092eee10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 3,
                "x": 34,
                "y": 42
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "05a4cfd9-e063-4809-b0b4-c5a2f5fd1de6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 93,
                "y": 42
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "6da7d2ab-9bae-4cb9-8cd6-54be63516a62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 25,
                "y": 42
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "5c17d737-34dd-4307-8ca3-c5d44d298a9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 4,
                "x": 11,
                "y": 42
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "54dc5e05-d00c-4db9-932a-57b03d5d91a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "0f907055-7330-4c73-8bc0-68da6706d8d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 18,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 248,
                "y": 22
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "201471cd-2ee5-41b2-ac34-e103d09a79d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 240,
                "y": 22
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "d846004b-4439-4e4e-9563-c7b5cacfaf7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 232,
                "y": 22
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "b5425f6d-5aa5-4222-a658-9bfed11c64bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 224,
                "y": 22
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "747bc9fa-3c79-4f9e-9d6c-632d1539c0cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 215,
                "y": 22
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "c1379170-2080-4491-9e08-19c29ab2aa56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 207,
                "y": 22
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "66112721-56dd-4477-b36c-013244637827",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 199,
                "y": 22
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "bd995786-9cd7-419f-ad98-6f79ccd2be28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 17,
                "y": 42
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "6f816b17-e278-46de-b817-a7dadb6254c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 101,
                "y": 42
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "010cf9e2-c838-4dfa-9dd8-99cfc7adab3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 109,
                "y": 42
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "aa7100cd-96e2-46d6-8126-c1496f517118",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 117,
                "y": 42
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "40fe0831-1fef-4c75-afbb-1929acf14e27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 69,
                "y": 62
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "2af3c7bb-9241-4470-a6e4-58466d1a09e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 18,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 65,
                "y": 62
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "9a83cdf7-ad8a-4769-8a08-c342b9f18da5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 4,
                "x": 59,
                "y": 62
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "db5951d2-8bf6-4179-bda3-301e453a0ac7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 50,
                "y": 62
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "dabf9fd0-1a23-4d84-bcb2-a89c4f2213b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 40,
                "y": 62
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "70b0a2f2-2fbe-4406-9185-6e00023d6391",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 30,
                "y": 62
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "b6aaef08-3ba5-4ba1-b939-8058e95e5f0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 22,
                "y": 62
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "a751495d-2f0f-40c9-b1a4-a88f42836eef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 14,
                "y": 62
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "fe74fd29-50e9-44f1-90d3-7f916e6fd75b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "0fd511f2-0b8c-4297-911a-004f9793f6f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 234,
                "y": 42
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "774bb58a-e3be-48ad-9d48-d45163c82e29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 224,
                "y": 42
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "97ea7c60-9179-406f-a9ce-8614b31d66f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 214,
                "y": 42
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "868b94d7-7d0a-4d1d-8edd-27e6f612cb2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 205,
                "y": 42
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "8ce7febe-d0d3-4b13-9338-17bf38b1de8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 195,
                "y": 42
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "72e9084a-b451-4611-a2e5-ba7d9881866b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 185,
                "y": 42
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "5eb09aa0-00ed-4b0d-ba11-d764357f4b91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 175,
                "y": 42
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "e105ccc9-6911-4247-a8d9-9ccd46e36b03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 167,
                "y": 42
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "1136a5fb-595c-4236-84f9-0c0624d791c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 157,
                "y": 42
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "0602c074-dee2-40ab-a17f-e9670d83ebd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 147,
                "y": 42
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "a6998443-b683-49ed-860e-51b1613b090e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 137,
                "y": 42
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "7ba5f313-9f3c-40b3-b32c-c7fa04b6255f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 125,
                "y": 42
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "51f0ae42-16d9-4195-b74a-1e0c898a3634",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 188,
                "y": 22
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "deb3663c-622c-4836-8c1b-df0cd6b659f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 178,
                "y": 22
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "697ab611-6056-4399-9295-29877ee20365",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 168,
                "y": 22
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "cba19f69-772c-4def-8ece-26a7a281039e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "390ebbe0-d8f3-4814-b018-5587bd1930f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "adb0830b-a4fb-4c55-8ace-bc4a94859fbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 180,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "16126d8a-42f5-4b0c-b2c7-59b7edcd7a1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 171,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "e76b908c-e0d3-4290-b2ed-57ff275632c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 161,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "47934961-c098-4de0-905f-7a65997a65e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "0d55a363-d48f-4191-a0d2-2bfca9a9ae1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 139,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "fc825639-0740-4484-8bdc-91cfd2a2da2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "3922dda0-35a4-4eee-b266-a2799a2b8c3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 119,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "7f0af31a-bd2e-4fbc-921b-de05eb2fd7ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "b3eb68ec-d57a-414e-969c-6af4c59ef06c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 18,
                "offset": 4,
                "shift": 10,
                "w": 3,
                "x": 199,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "52cba4e7-c37c-4d75-884c-c775e6a1564c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "603ea27f-c425-49e2-9ddb-7f6769fe42d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 3,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "eab426ce-16cc-4c72-84c0-3c1d3fbbaf8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "543f9e55-2baf-4320-a84e-b82122fcd6bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "1ec38241-0a1e-45b1-a4b1-a1bb0222c109",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 18,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "2d97f5f3-7695-4abb-9d9c-66b57fe7293f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "897f7aab-d928-4322-898c-c400b13a7bbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "5b6c4930-bb48-42e7-9019-9794c108e6b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "98458b05-cb67-48fe-9f81-aac4711b13cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "360c1025-d9e8-465a-909c-9aef9d60df68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "16f01c23-953e-494c-8f76-e0e61abdad0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "dfc45e1a-56a9-4164-904a-31bc558f8b36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 214,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "e9086e7d-3c37-4584-887d-061c91e8e03c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 54,
                "y": 22
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "a5fad574-185c-42bd-baa7-2fb7a9c88fb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 224,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "959e9ed9-6e55-4242-b1f7-c48ed530e58f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 5,
                "x": 153,
                "y": 22
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "5b2f4bd9-5b6b-421e-a6fd-79aa336404cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 144,
                "y": 22
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "b1bc26a7-6469-4791-9b44-8d68fa092355",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 135,
                "y": 22
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c7884fa6-aec6-4b1a-9fe2-ae2659f6d847",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 123,
                "y": 22
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "e30655ef-a9b7-447c-b9a4-ff00305eff76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 113,
                "y": 22
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "f72cbf21-7eb0-45e0-ae2e-8dfddaea4733",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 104,
                "y": 22
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "ef748f64-6df7-441c-94f7-6d299e9c6c8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 93,
                "y": 22
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "d91acfa9-45da-4ebc-b910-51d896a514d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 83,
                "y": 22
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "8644001d-c84c-428e-9c70-1936e9a29a2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 73,
                "y": 22
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "f0a760b2-ee47-431a-97e9-28abef1c10e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 160,
                "y": 22
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "3a4272d8-175b-4961-9d63-d5c3e6ecd0ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 64,
                "y": 22
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "c5bb180c-4caa-4577-8448-f1d861ec58fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 44,
                "y": 22
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "f4b7aad4-1d5c-4734-9193-338551d8813e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 33,
                "y": 22
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "e898bca4-3afc-486f-9d29-24da9f1492f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 22,
                "y": 22
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "9eadfbdf-6018-4828-a427-b672592f1961",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 12,
                "y": 22
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "ee4dda6e-69e7-4384-b81f-924a79de834f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "aa8bc89e-8e96-45de-8351-865d6dd407ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 247,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "ec442438-a353-4633-8425-010553ea1172",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 18,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 242,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "da1e2354-a5c5-4f35-921e-428bd9c36e4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 18,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 238,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "fb783f79-c542-4e92-b8fb-e70a4b473ae2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 18,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 233,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "1ac1b6d0-ef7c-46b9-99df-6ae367074889",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 77,
                "y": 62
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "458e652b-cff1-4998-9246-f733ee079b6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 18,
                "offset": 3,
                "shift": 16,
                "w": 9,
                "x": 86,
                "y": 62
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}
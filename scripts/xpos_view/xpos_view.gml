//@decription Get the x position of an object
//@arg x1
//@arg x2

//X position of object
var x1 = argument0;


x1 = x1 - camera_get_view_x(view_camera[0]);

// Return position of object in view
return x1;
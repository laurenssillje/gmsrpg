//@desc move actor
//@param direction
var dir = argument0;

if(states == states.idle) {
	
	if(sprite_index != sprite[dir]) {
		states = states.turning;
		sprite_index = sprite[dir];
	} else {
		walk(dir);	
	}
}
gml_pragma("global", "MACRO()");

#macro tile_width 16;
#macro tile_height 16;
#macro tile_size = 16;

enum directions {
	right,
	up,
	left,
	down
	
}
global.components = [];
global.components[directions.right] = [1,0];
global.components[directions.up] = [0,-1];
global.components[directions.left] = [-1,0];
global.components[directions.down] = [0,1];
//@decription Get the x position of an object
//@arg x1
//@arg x2

//X position of object
var y1 = argument0;


y1 = y1 - camera_get_view_y(view_camera[0]);

// Return position of object in view
return y1;
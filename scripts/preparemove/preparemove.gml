//@desc move actor
//@param direction

var dir = argument0;

var time = 0;
var maxTime = 0.1;
var press = false;
states = states.premove;



while(time <= maxTime) {
	enteredLoop = true;
	if(!lastkey[dir]) {
		show_debug_message("We should turn now (or walk in case same direction)");
		press = true;
		break;
	}
	time+=delta_time/1000;
}



if(press) {
	states = states.idle;
	turn(dir);	
} else if(!press) {
	states = states.idle;
	walk(dir);
}
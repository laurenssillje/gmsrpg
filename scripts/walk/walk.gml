//@desc move actor
//@param direction

var dir = argument0;
var components = global.components[dir];
var dx = components[0];
var dy = components[1];

if (states == states.idle) {
	if (!tilemap_get(tile_map,x_pos + dx, y_pos+dy)) {
	
		x_from = x_pos;
		y_from = y_pos;

		x_to = x_pos + dx;
		y_to = y_pos + dy;

		x_pos = x_to;
		y_pos = y_to;

		states = states.walking;
		sprite_index = sprite[dir];
		
	} else if (tilemap_get(tile_map,x_pos + dx, y_pos+dy)) {
		//if not looking at current dir turn(dir);	
	}
	recInput = false;
	recRelease = false;
}
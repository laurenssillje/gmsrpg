//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;
uniform float itime;
uniform sampler2D wavetex;

void main()
{
	
	
	//gl_FragColor.a = itime;
    gl_FragColor = /*v_vColour * texture2D( gm_BaseTexture , v_vTexcoord ) +*/ v_vColour * texture2D(wavetex, v_vTexcoord);
	gl_FragColor.a = itime;
	
	//0.2,0.92,0.92
	//gl_FragColor = vec4(0.2,0.92,0.92,itime);
}

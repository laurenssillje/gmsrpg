{
    "id": "e7a8aa65-e48a-4897-b958-fa548c58e569",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_transition",
    "eventList": [
        {
            "id": "62f98c8e-d69f-4355-8d48-1bb50074efe2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e7a8aa65-e48a-4897-b958-fa548c58e569"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "b258dd86-02f1-4404-a8ba-e83cb50aaa31",
    "visible": false
}
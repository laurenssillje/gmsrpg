{
    "id": "edf1e278-3819-4d87-bcc5-85ca4a942c16",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_sign",
    "eventList": [
        {
            "id": "224d80d8-b70b-4b32-91b9-c6bf9e3c0090",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "edf1e278-3819-4d87-bcc5-85ca4a942c16"
        },
        {
            "id": "967804b8-4161-4e01-be81-e3924b9a4b6d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "edf1e278-3819-4d87-bcc5-85ca4a942c16"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2699faf1-5a67-4517-9538-bf885bc6a9bb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "34cfdf6c-20d4-4bc8-9f3c-722370c43757",
    "visible": false
}
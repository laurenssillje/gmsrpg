{
    "id": "0d1d80b0-ce1e-448d-93af-ebfcabb4c6ce",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_event",
    "eventList": [
        {
            "id": "d7e1c9a1-680f-45eb-9f57-bec0fbb88f8b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0d1d80b0-ce1e-448d-93af-ebfcabb4c6ce"
        },
        {
            "id": "db17ee82-c19c-4480-aa04-1c5fc561e12c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0d1d80b0-ce1e-448d-93af-ebfcabb4c6ce"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "433ade73-9c27-4a01-923a-9b2c61f89f3d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "57eeef40-f5e7-4523-ad40-954e74178189",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 0
        },
        {
            "id": "296bda8c-7508-43c1-96bd-d1ce5721d3a3",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        },
        {
            "id": "47b6231c-acc4-4418-a66e-276320005884",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9a6494d0-efee-4b48-a685-ebe826132682",
    "visible": false
}
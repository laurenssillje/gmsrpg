/// @description Setup variables


//Meta stuff
hasFired = false;
doOnce = true;
LockPlayer = true;
requiredDirection = -1;
touchEvent = false;

//Event types
isTileChanger = -1;
isSpriteShower = -1;
isDialogue = -1;


//Variables

//TILE EVENT
targetLayer = -1;
targetX = -1;
targetY = -1;
targetTile = -1;


//SPRITE EVENT
spriteToShow = -1;
beginX = -1;
beginY = -1;
endX = -1;
endY = -1;


//DIALOGUE EVENT
message[0] = "|0a";
portrait = "none";

/// @description Event code

//TILE CHANGE EVENT

var lay_id = layer_get_id(targetLayer);
var map_id = layer_tilemap_get_id(lay_id);

//check for player collision
if(instance_place(x,y,obj_player) != noone and obj_player.facing = requiredDirection) {
	touchEvent = true;	
} else {
	touchEvent = false;	
}


if(isTileChanger && !hasFired && touchEvent) {
	
	
	tilemap_set(map_id, targetTile, targetX, targetY);
	
	hasFired = true;
}



//DIALOGUE EVENT

if(isDialogue && !hasFired && touchEvent) {

	global.talking = true;
	script_execute(dialogue,message,portrait);
	
	
	if(doOnce) {
	hasFired = true;	
	} else {
	hasFired = false;	
	}


}
/// @description Draw menu

draw_set_halign(fa_left);
draw_set_valign(fa_middle);
draw_set_font(f_dialogue);
draw_set_color(c_white);

if(room == room_menu) {
if(selected == 0) {

	draw_text_color((room_width / 2) + string_width(menu[0]) - offset  ,(view_h/2)+offset,menu[0],c_s,c_s,c_s,c_s,1);
	draw_text((room_width / 2)+string_width(menu[1]) + offset ,(view_h/2)+offset,menu[1]);
}


if(selected == 1) {

	draw_text((room_width / 2)+string_width(menu[0]) - offset,(view_h/2)+offset,menu[0]);
	draw_text_color((room_width / 2)+string_width(menu[1]) + offset,(view_h/2)+offset,menu[1],c_s,c_s,c_s,c_s,1);
}
}


if (room == room_controls) {
	
	for (i=0; i < array_length_1d(controls); i++) {
		
		draw_text((room_width / 2)-offset,(view_h/2) + (i*space),controls[i]);
		
	}
	
}






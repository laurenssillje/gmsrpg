
menu[0] = "START";
menu[1] = "RESET";

controls[0] = " --- CONTROLS ---";
controls[1] = "Z or E to confirm/interact";
controls[2] = "X to advance dialogue or cancel";
controls[3] = "WASD / Arrows to move";
controls[4] = " ";
controls[5] = "Press any key to begin the game";

offset = 32
space = 16;
selected = 0;
c_s = c_orange;


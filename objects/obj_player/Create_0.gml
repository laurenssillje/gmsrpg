/// @description

//player states
enum states {
	idle,
	premove,
	walking,
	transition,
	talking,
	turning,
}


/*
DEBUGGING
*/
enteredLoop = false;

last_dir = -1;
moveDelay = 10//in frames > 10/60 = 0,167 sec.
recInput = false;
recRelease = false;


states = states.idle;

last_pressed = -1;


//Current positions
x_pos = x div tile_width;
y_pos = y div tile_height;

//What position we are moving from
x_from = x_pos;
y_from = y_pos;

//Position we are moving to
x_to = x_pos;
y_to = y_pos;

walk_anim_length = 0.5;
walk_anim_time = 0;
turn_anim_time = 0;

image_speed = 0;



//Used to determine next frame in the walk cycle
walk_frame = 1;
sprite[directions.right] = spr_player_right;
sprite[directions.left] = spr_player_left;
sprite[directions.up] = spr_player_up;
sprite[directions.down] = spr_player_down;

var tile_layer = layer_get_id("Walls");
tile_map = layer_tilemap_get_id(tile_layer);
//player variables

//shadow


facing = 0;
debuginst = -1;

input_interact = keyboard_check_pressed(ord("Z")) or keyboard_check_pressed(ord("E")) or keyboard_check_pressed(ord("X"));
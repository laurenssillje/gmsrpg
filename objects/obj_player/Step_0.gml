/// @description
//input checks

depth = room_height - y - 16; //-6 because the origin is 6 pixels above the feet of the actors

input_interact = keyboard_check_pressed(ord("Z"));
input_left = keyboard_check(vk_left);
input_right = keyboard_check(vk_right);
input_up = keyboard_check(vk_up);
input_down = keyboard_check(vk_down);

releaseKey[directions.right] = keyboard_check_released(vk_right);
releaseKey[directions.left] = keyboard_check_released(vk_left);
releaseKey[directions.up] = keyboard_check_released(vk_up);
releaseKey[directions.down] = keyboard_check_released(vk_down);


if(states == states.idle) {
	
	if(!recInput) {
		if(input_left) {
			recInput = true;
			last_dir = directions.left;
		}
		
		if(input_right) {
			recInput = true;
			last_dir = directions.right;
		}
		
		if(input_down) {
			recInput = true;
			last_dir = directions.down;
		}
		
		if(input_up) {
			recInput = true;
			last_dir = directions.up;
		}
	}
	
	
	if(recInput && moveDelay >= 0 && !recRelease) {
		if(releaseKey[last_dir]) {
			recRelease = true;
			turn(last_dir);
		}
		moveDelay--;		
	}
	
	if(recInput && moveDelay <= 0 && !recRelease) {
		walk(last_dir);	
	} 
	
	if(keyboard_check(vk_nokey)) {
		moveDelay = 10;	
	}
	
}

//WALKING
if(states == states.walking) {
	
	if(walk_frame > 2) {
		walk_frame = 1;	
	}
	
	walk_anim_time += delta_time / 1000000;
	
	var t = walk_anim_time / walk_anim_length;
	
	if(t >= 1) {
		recInput = false;
		recRelease = false;
		//moveDelay = 10;
		walk_anim_time = 0;
		t = 1;
		states = states.idle;
		image_index = 0;
		walk_frame++;
	}	
	
	
	var _x = lerp(x_from,x_to,t);
	var _y = lerp(y_from, y_to, t);
	
	x = _x * tile_width;
	y = _y * tile_height;
	
	if(t > 0.3 && t < 0.6) {
		image_index = walk_frame;
	}
	
}


//TURNING
if(states == states.turning) {
	
	turn_anim_time += delta_time / 1000000;
	var time = turn_anim_time / 0.3;
	
	if(time >= 1) {
	recInput = false;
	recRelease = false;
	moveDelay = 10;
	turn_anim_time = 0;
	time = 1;
	states = states.idle;
	image_index = 0;
	}
	
	if(time > 0.2 && time < 0.8) {
		image_index = 1;	
	}
	
}



//interact
with(obj_sign){ 
	if(collision_line(obj_player.x, obj_player.y, obj_player.x + lengthdir_x(18, obj_player.direction), obj_player.y + lengthdir_y(18, obj_player.direction), self, 1, 0) && obj_player.input_interact && global.lockplayer = false)
    {	
		global.lockplayer = true;
		show_debug_message("Shown dialogue");
		script_execute(dialogue,message,portrait);
	}
}

//Check for transition objects
var inst = instance_place(x,y,obj_transition);
if(inst!=noone){
debuginst = true;	
} else {
debuginst = false;	
}

if(input_interact){ 	
if(inst != noone and facing == inst.playerFacingBefore) {
	
	with(game) {

	if(!doTransition) {
	
	spawnRoom = inst.targetRoom;
	spawnX = inst.targetX;
	spawnY = inst.targetY;
	spawnPlayerFacing = inst.playerFacingAfter;
	doTransition = true;
			}
		}
	}
}

/*
//Use ALARMS to check button presses
	if(input_any) {
	
	if(input_left) {
		last_pressed = input_left;	
		inputToDirection = directions.left;
	}
	
	if(input_right) {
		last_pressed = input_right;	
		inputToDirection = directions.right;
	}
	
	if(input_up) {
		last_pressed = input_up;	
		inputToDirection = directions.up;
	}
	
	if(input_down) {
		last_pressed = input_down;	
		inputToDirection = directions.down;
	}
	
	
	
	 Wait for 0.3 seconds then check for button release
	while(releaseTimer < 0.15) {
	releaseTimer+=delta_time/1000000;
	if(input_left_r) {
		last_pressed = input_left_r;
		inputToDirection = directions.left;
		performTurn = true;
		break;
	}
	
	if(input_right_r) {
		last_pressed = input_right_r;
		inputToDirection = directions.right;
		performTurn = true;
		break;
	}
	
	if(input_down_r) {
		last_pressed = input_down_r;	
		inputToDirection = directions.down;
		performTurn = true;
		break;
	}
	
	if(input_up_r) {
		last_pressed = input_up_r;
		inputToDirection = directions.up;
		performTurn = true;
		break;
	}
	}
}
}

if(performTurn && last_pressed != -1){
	turn(inputToDirection);	
} else if(last_pressed != -1 && !performTurn) {
	walk(inputToDirection);
*/
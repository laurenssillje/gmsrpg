/// @description 

//todo: add portraits/name
sbox = spr_textbox;
//sframe = spr_frame; same as portrait
//sportrait = spr_portrait;
//sname = spr_namebox;


box_width = sprite_get_width(sbox);
box_height = sprite_get_height(sbox);


port_width = 120;
port_height = 120;

//namebox_width = sprite_get_width(sname);
//namebox_height = sprite_get_height(sname);

offset = 4; //temporary

port_x = (global.game_width - box_width - port_width)*0.5;
port_y = (global.game_height*1.05) - port_height;

if(portrait != "none"){
box_x = port_x + port_width;
} else { box_x = (global.game_width*0.5)-190; }

box_y = port_y;
namebox_x = port_x + offset;
//namebox_y = box_y - namebox_height + offset;


input_act = keyboard_check_pressed(ord("X")) or keyboard_check_pressed(ord("F"));
draw_set_font(f_dialogue);
//draw_set_colour(c_white);

message_end = array_length_1d(message); //last message to show

t++;
if (message_end > 0) {




//text variables

var charWidth = 11; //a bit smaller than the size of the font
var lineEnd = 30; // > creation event > amount of chars in a single line prev 30
var line = 0; //current line
var space = 0; //character count (first letter is 0, second is 1, etc)
var i = 1; //looping purpose
var delay = 1; //delay between each character
var typing; //check if there's still letters being typed out

//text_wrapped = str_wrap(message[message_current],lineEnd);
//str_len = string_length(text_wrapped);

//check if sentence doesnt start with a space



//Typewriter
if (cutoff < string_length(message[message_current])) {
	
	typing = true;
	
	if (timer >= delay) {
	cutoff++;
	
	timer = 0;
	
	} else { timer++; }
	
} else { typing = false; }


//complete message when we press a key while it's still typing out
if (typing && input_act) {
	cutoff = string_length(message[message_current]);	
	typing = false;
} else if (input_act && typing = false) {
	
		//go to next message if we arent at the last one yet
	if (message_current < message_end-1) {
		message_current++;
		cutoff = 0;
		
	}
	
	//else were done
	else {
	done = true;
	instance_destroy();	
	} 

	}
	
	

}




	//draw textbox and co
	//TODO: add if statements
	
	
	
	if(portrait == "none") {
	//draw_sprite(sname,0,namebox_x,namebox_y);
	//draw_sprite(sportrait,0,port_x + offset,port_y);
	draw_sprite(sbox,0,box_x,box_y);
	} 
	
	//text buffers
	x_buffer = 16;
	y_buffer = 12
	text_x = box_x + x_buffer;
	text_y = box_y + y_buffer;
	//name_text_x = namebox_x + (namebox_width/2);
	//name_text_y = namebox_y + (namebox_height/2);

	
	
	//draw name if there is one
	if(portrait!="none"){
	draw_set_halign(fa_center); draw_set_valign(fa_middle);
	//draw_text_color(name_text_x,name_text_y,name,namecol,namecol,namecol,namecol,1);
	draw_set_halign(fa_left); draw_set_valign(fa_top);
	}
	//draw the text
	
	


	//var last_space = 1;
	//var count = 1;
	//repeat(string_length(message[message_current])) {
		
	//		if(string_char_at(message[message_current],count) == " " ){ last_space = count; } 
	//		if(string_width(message[message_current]) > lineEnd) {
	//		string_insert("\n",message[message_current],last_space);	
	//		}
			
	//		count++;
			
	//}
	

	
	
	while (i <= string_length(message[message_current]) && i <= cutoff) {

			
			
			
			
			
			
		
			//check for text modifier
			if (string_char_at(message[message_current],i) == "|") {
				modifier = real(string_char_at(message[message_current], ++i));
				++i;
			}
			
			if (string_char_at(message[message_current],i) == "/") {
				++i;
				line++;
				space = -1;
			}
			
			if(string_char_at(message[message_current],i) == " " && space > 24)  {
				line++;
				space = -1;
			}
			
			
			
			//list of modifiers
			switch(modifier) {
					case 0: //no effect
					{
					draw_set_colour(c_white);
					draw_text(text_x+(space*charWidth),text_y+(13*line),string_char_at(message[message_current], i));
					break;
					}
					
					case 1: //shaky
					{
					
					draw_set_colour(c_white);
					draw_text(text_x+(space*charWidth)+random_range(-1,1),text_y+(13*line)+random_range(-1,1),string_char_at(message[message_current], i));
					break;
					}
					
					case 2: //wavy
					{
				
					var so = t+i;
					var shift = sin(so*pi*freq/room_speed)*amplitude;
					draw_set_colour(c_white);
					draw_text(text_x+(space*charWidth),text_y+(13*line)+shift,string_char_at(message[message_current], i));
					break;
					}
					
					case 3: //new line
					{
						break;
					}
			}
			i++;
			space++;
	}
	
	
	//add voices
	switch(voice) {
		case "none":
		{
			break;	
		}
		
		case "player":
		{
		//TODO: add voices
		break;
		}
	}
	
	//draw portrait
	switch(portrait) {
	
	case "none":
		{
			break;	
		}
		
	case "playerchar":
		{
			//draw_sprite(spr_p_playerchar, 0 , 5 ,view_hview[0]-55);
			break;
		}
	
	}

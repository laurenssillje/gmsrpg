{
    "id": "659c1363-bfcf-4827-8cdb-4a7d2853effc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_display_manager",
    "eventList": [
        {
            "id": "8e0778b7-f69f-484f-b697-5c6c8c742980",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "659c1363-bfcf-4827-8cdb-4a7d2853effc"
        },
        {
            "id": "6ed269ab-7689-40c9-8dc5-2e5c05c45536",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "659c1363-bfcf-4827-8cdb-4a7d2853effc"
        },
        {
            "id": "547160df-9d0c-4d6c-b241-e309843b9a54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "659c1363-bfcf-4827-8cdb-4a7d2853effc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
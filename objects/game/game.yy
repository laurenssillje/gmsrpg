{
    "id": "3814831c-34ba-4902-87c3-a30ee9aa336b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "game",
    "eventList": [
        {
            "id": "457ea9a8-b2a1-4dfb-9014-13f525760593",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3814831c-34ba-4902-87c3-a30ee9aa336b"
        },
        {
            "id": "ce27b452-57fe-4e72-8792-ddc08996d311",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3814831c-34ba-4902-87c3-a30ee9aa336b"
        },
        {
            "id": "c286cc07-f777-4d43-bef3-d0822020ab2d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "3814831c-34ba-4902-87c3-a30ee9aa336b"
        },
        {
            "id": "5381299b-97b4-4105-8b7e-7256224113ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "3814831c-34ba-4902-87c3-a30ee9aa336b"
        },
        {
            "id": "cd098cef-653a-440b-8121-bf137f8e7826",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "3814831c-34ba-4902-87c3-a30ee9aa336b"
        },
        {
            "id": "827b19e6-ea92-4231-b59c-15555cefc954",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3814831c-34ba-4902-87c3-a30ee9aa336b"
        },
        {
            "id": "e05a444c-5423-4180-84d1-7fb1d4251976",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3814831c-34ba-4902-87c3-a30ee9aa336b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
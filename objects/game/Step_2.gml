/// @description Insert description here
#macro view view_camera[0]
camera_set_view_size(view,view_width,view_height);
//var _spd = .1;
//var _cur_x = camera_get_view_x(view);
//var _cur_y = camera_get_view_y(view);

if(instance_exists(obj_player)) {

	var _x = clamp(obj_player.x - view_width/2,0,room_width-view_width);
	var _y =  clamp(obj_player.y - view_height/2,0,room_height-view_height);

	camera_set_view_pos(view,_x, _y);
	
	} else {

	camera_set_view_pos(view,0,0);

}
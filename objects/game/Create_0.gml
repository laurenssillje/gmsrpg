//spawnedTrans = false;
//shaking = false;

//save_data=ds_map_create();
//global.gamestate = 0; //Overworld, 1 = Battle
firstlaunch = true;
global.lockplayer = false;

/// @description Variables (add debug check)
game_width = obj_display_manager.ideal_width;
game_height = obj_display_manager.ideal_height;
global.game_width = obj_display_manager.ideal_width;
global.game_height = obj_display_manager.ideal_height;

//display_set_gui_size(global.game_width,global.game_height);
//global.playername = "playername";
spawnRoom = -1;

doTransition = false;

spawnX = 0;

spawnY = 0;

spawnPlayerFacing = -1;

roomHasEvents = false;

playercurrentdir = -1;

debug = false;


enum dir {
right = 0,
up = 90,
left = 180,
down = 270,
}

#macro cam view_camera[0]
#macro view_w camera_get_view_width(view_camera[0])
#macro view_h camera_get_view_height(view_camera[0])
aspect_ratio = display_get_width()/display_get_height();

view_height= game_height/2;
view_width=round(view_height*aspect_ratio);
 
if(view_width & 1) view_width++;
if(view_height & 1) view_height++;

max_window_scale = min(floor(display_get_width()/view_width),floor(display_get_height()/view_height));
if(view_height * max_window_scale == display_get_height())
    max_window_scale--;
    
window_scale = max_window_scale;

window_set_size(view_width*window_scale,view_height*window_scale);
alarm[0]=1;



surface_resize(application_surface,view_width*window_scale,view_height*window_scale);






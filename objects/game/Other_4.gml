/// @description move player

scr_tile_deepen("Depth", 16, 16, room_height);

lid = layer_get_id("Depth");
layer_destroy(lid);


view_enabled = true;
view_visible[0] = true;

if(instance_exists(obj_player)){

var __x = clamp(obj_player.x - view_width/2,0,room_width-view_width);
var __y = clamp(obj_player.y - view_height/2,0,room_height-view_height);

camera_set_view_pos(view,__x,__y);
	
}

if(instance_exists(obj_event)) {
roomHasEvents = true;	
} else {
roomHasEvents = false;	
}


if(instance_exists(obj_player)) {
if(spawnRoom == -1) exit;
obj_player.x = spawnX;
obj_player.y = spawnY;
obj_player.facing = spawnPlayerFacing;

with(obj_player) {
	
	switch(facing){
	case dir.left: sprite_index = spr_player_left; break;
	case dir.right: sprite_index = spr_player_right; break;
	case dir.up: sprite_index = spr_player_up; break;
	case dir.down: sprite_index = spr_player_down; break;
	case -1: image_speed = 0; image_index = 1; break;
}
	
}
}

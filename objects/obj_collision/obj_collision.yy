{
    "id": "2699faf1-5a67-4517-9538-bf885bc6a9bb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_collision",
    "eventList": [
        {
            "id": "52d9fea1-e0a8-4343-8f25-8108e47dfbee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2699faf1-5a67-4517-9538-bf885bc6a9bb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ed2509a9-07a8-47bc-b2a9-e1598c52b5d3",
    "visible": false
}
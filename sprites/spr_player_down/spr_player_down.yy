{
    "id": "f4c5dbea-fb9f-4801-b6fb-2f185493c816",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2aa713ed-1bb2-4132-b24d-c7deaa8bb0cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4c5dbea-fb9f-4801-b6fb-2f185493c816",
            "compositeImage": {
                "id": "0d168986-e3b9-4d55-8c9c-b9492ec4f1c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2aa713ed-1bb2-4132-b24d-c7deaa8bb0cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee8539c7-8cd2-4569-af56-4b50cfe6a4a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2aa713ed-1bb2-4132-b24d-c7deaa8bb0cd",
                    "LayerId": "1ee1ed94-4e23-4197-8166-51fa202dc41a"
                }
            ]
        },
        {
            "id": "c3940b59-9459-4c7f-ade4-4882b1cbb1ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4c5dbea-fb9f-4801-b6fb-2f185493c816",
            "compositeImage": {
                "id": "14039ad3-dfb0-4a60-b776-21822551a9a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3940b59-9459-4c7f-ade4-4882b1cbb1ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8980ca34-c1fa-455e-811c-2eae6758281a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3940b59-9459-4c7f-ade4-4882b1cbb1ac",
                    "LayerId": "1ee1ed94-4e23-4197-8166-51fa202dc41a"
                }
            ]
        },
        {
            "id": "8ced5903-d0d6-41eb-9871-3261e6859967",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4c5dbea-fb9f-4801-b6fb-2f185493c816",
            "compositeImage": {
                "id": "0239a46f-b910-41c4-bf2c-3b283052f3ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ced5903-d0d6-41eb-9871-3261e6859967",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0e7cc71-5272-4bd9-9da3-6b26d4fbb532",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ced5903-d0d6-41eb-9871-3261e6859967",
                    "LayerId": "1ee1ed94-4e23-4197-8166-51fa202dc41a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "1ee1ed94-4e23-4197-8166-51fa202dc41a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f4c5dbea-fb9f-4801-b6fb-2f185493c816",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 0,
    "yorig": 6
}
{
    "id": "4bec5b65-48c3-4a47-88cd-32fe2b22acf3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_transition_squares",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 282,
    "bbox_left": 0,
    "bbox_right": 499,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5fa31cfd-dc1a-499b-b3b5-bc6c283a73ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bec5b65-48c3-4a47-88cd-32fe2b22acf3",
            "compositeImage": {
                "id": "d20caf5b-7aa3-4cd5-abde-d2af86d4ad5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fa31cfd-dc1a-499b-b3b5-bc6c283a73ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7be317d0-9d2d-4f75-a97b-4118be81f117",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fa31cfd-dc1a-499b-b3b5-bc6c283a73ff",
                    "LayerId": "5123d35b-2588-4492-87c6-9dd2b7df0231"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 283,
    "layers": [
        {
            "id": "5123d35b-2588-4492-87c6-9dd2b7df0231",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4bec5b65-48c3-4a47-88cd-32fe2b22acf3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 500,
    "xorig": 0,
    "yorig": 0
}
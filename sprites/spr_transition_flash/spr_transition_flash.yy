{
    "id": "5a9dac44-7b9a-4a08-8a44-1acbeaf8363b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_transition_flash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 271,
    "bbox_left": 0,
    "bbox_right": 479,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4fb1cc0f-2f6d-4bfb-afb9-8f4c883717be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a9dac44-7b9a-4a08-8a44-1acbeaf8363b",
            "compositeImage": {
                "id": "648f8b6c-9695-441c-bfa6-8b0a33dada9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fb1cc0f-2f6d-4bfb-afb9-8f4c883717be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "780704db-7447-440e-a8d6-e499cc4f063b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fb1cc0f-2f6d-4bfb-afb9-8f4c883717be",
                    "LayerId": "eec5e74f-b4ed-4ef8-968a-af49f3915826"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 272,
    "layers": [
        {
            "id": "eec5e74f-b4ed-4ef8-968a-af49f3915826",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a9dac44-7b9a-4a08-8a44-1acbeaf8363b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 0,
    "yorig": 0
}
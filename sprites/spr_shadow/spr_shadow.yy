{
    "id": "04770209-3fd6-43de-a2d0-e6cd7a4e71d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb4c7fdf-5233-4be5-96dd-a04f044e3fe6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04770209-3fd6-43de-a2d0-e6cd7a4e71d9",
            "compositeImage": {
                "id": "cac8553c-6479-427d-9f12-2c5352a7a5b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb4c7fdf-5233-4be5-96dd-a04f044e3fe6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ca3cf64-82db-4241-a03d-b8b004cb57d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb4c7fdf-5233-4be5-96dd-a04f044e3fe6",
                    "LayerId": "6a7bed7a-ea34-4746-9f02-f6b710562b9f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "6a7bed7a-ea34-4746-9f02-f6b710562b9f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "04770209-3fd6-43de-a2d0-e6cd7a4e71d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 3
}
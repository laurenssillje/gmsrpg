{
    "id": "dcceeb42-d426-4847-bd6f-2fedea307773",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lightcol",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db49822a-441c-4049-b206-7f6e4f429fc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcceeb42-d426-4847-bd6f-2fedea307773",
            "compositeImage": {
                "id": "35da4d21-7646-4386-8940-292ae730a8d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db49822a-441c-4049-b206-7f6e4f429fc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56f3dadf-c1fe-4251-bb18-14b4641d0ab2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db49822a-441c-4049-b206-7f6e4f429fc5",
                    "LayerId": "2ff52b4a-a798-418d-92ce-9c320388e4f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2ff52b4a-a798-418d-92ce-9c320388e4f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dcceeb42-d426-4847-bd6f-2fedea307773",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "b258dd86-02f1-4404-a8ba-e83cb50aaa31",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_transition",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "82d077fc-080c-47bd-9a3a-65d81ed41c62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b258dd86-02f1-4404-a8ba-e83cb50aaa31",
            "compositeImage": {
                "id": "6e1a2189-6082-4de1-8b4e-52e886fcf7fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82d077fc-080c-47bd-9a3a-65d81ed41c62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f787e059-390f-48ec-8119-0f790848e12c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82d077fc-080c-47bd-9a3a-65d81ed41c62",
                    "LayerId": "cad2ad7d-34a5-4a63-8d21-6eca9b69e999"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cad2ad7d-34a5-4a63-8d21-6eca9b69e999",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b258dd86-02f1-4404-a8ba-e83cb50aaa31",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
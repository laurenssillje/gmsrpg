{
    "id": "ed2509a9-07a8-47bc-b2a9-e1598c52b5d3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_collision",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a465a1f7-ad18-4ad9-9e91-2be30fff5884",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed2509a9-07a8-47bc-b2a9-e1598c52b5d3",
            "compositeImage": {
                "id": "3d765dee-a2ce-4d10-a072-d16abdf79a49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a465a1f7-ad18-4ad9-9e91-2be30fff5884",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31d5eb2e-bf1a-4269-8ea5-57ed69ac2eff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a465a1f7-ad18-4ad9-9e91-2be30fff5884",
                    "LayerId": "29a19a08-27f9-41a0-817e-9b7368bd18df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "29a19a08-27f9-41a0-817e-9b7368bd18df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed2509a9-07a8-47bc-b2a9-e1598c52b5d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "3590650e-db38-4407-b3be-10dbe7aac582",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5f06a600-92a7-4fcb-a07f-476973b44466",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3590650e-db38-4407-b3be-10dbe7aac582",
            "compositeImage": {
                "id": "b4702463-53e5-4210-8481-5811ba2b8520",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f06a600-92a7-4fcb-a07f-476973b44466",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddd1fd82-cc5e-4129-bf3e-1b8deb8d73bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f06a600-92a7-4fcb-a07f-476973b44466",
                    "LayerId": "ee1ea633-a2e0-4a9f-ba16-0bf749515cb5"
                }
            ]
        },
        {
            "id": "eb903246-0920-4335-8425-932865f7794c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3590650e-db38-4407-b3be-10dbe7aac582",
            "compositeImage": {
                "id": "6ff8e75b-f8ff-4d41-8e50-b31e99d5c2d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb903246-0920-4335-8425-932865f7794c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "167a1285-c3cd-42ff-bda7-638815c9073a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb903246-0920-4335-8425-932865f7794c",
                    "LayerId": "ee1ea633-a2e0-4a9f-ba16-0bf749515cb5"
                }
            ]
        },
        {
            "id": "76d8d863-8fcf-4a95-8f3f-e4229591fd2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3590650e-db38-4407-b3be-10dbe7aac582",
            "compositeImage": {
                "id": "9eaebb18-e5f1-4e0f-8c4f-605ddd7d9c2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76d8d863-8fcf-4a95-8f3f-e4229591fd2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afe245cd-fab0-4c31-8f7f-0bfbef0d3973",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76d8d863-8fcf-4a95-8f3f-e4229591fd2c",
                    "LayerId": "ee1ea633-a2e0-4a9f-ba16-0bf749515cb5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "ee1ea633-a2e0-4a9f-ba16-0bf749515cb5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3590650e-db38-4407-b3be-10dbe7aac582",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 0,
    "yorig": 6
}
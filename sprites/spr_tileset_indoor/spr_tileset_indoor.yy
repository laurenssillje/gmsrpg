{
    "id": "f5730c90-9291-4a77-af2b-8943f5536d02",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset_indoor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "54d5ad44-fd8a-4ece-9974-84991ad92f09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5730c90-9291-4a77-af2b-8943f5536d02",
            "compositeImage": {
                "id": "40a66bf0-45fb-4da0-bf6f-1fdfc03eb71f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54d5ad44-fd8a-4ece-9974-84991ad92f09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d65a129c-2203-4075-88ea-09b86a3db8e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54d5ad44-fd8a-4ece-9974-84991ad92f09",
                    "LayerId": "d86dafbc-dc94-4275-b5f2-1a91080d5281"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "d86dafbc-dc94-4275-b5f2-1a91080d5281",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5730c90-9291-4a77-af2b-8943f5536d02",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": -33,
    "yorig": 107
}
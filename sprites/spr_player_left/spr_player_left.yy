{
    "id": "207a0dcb-f9bc-43b9-8205-5045c801e101",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ef12a955-8142-45fe-bed9-6c3c0f4317b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "207a0dcb-f9bc-43b9-8205-5045c801e101",
            "compositeImage": {
                "id": "acc88efc-21ff-4805-8d11-e662dd225dd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef12a955-8142-45fe-bed9-6c3c0f4317b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a43436db-b9b4-4ca6-95d2-ac97c0ef3083",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef12a955-8142-45fe-bed9-6c3c0f4317b5",
                    "LayerId": "9bde5830-0da2-46c9-abdb-a3e60230577c"
                }
            ]
        },
        {
            "id": "196783e6-0e56-4cce-b83b-441374c30a32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "207a0dcb-f9bc-43b9-8205-5045c801e101",
            "compositeImage": {
                "id": "0dc3f340-2fa1-4679-ba6c-f1921c2a04c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "196783e6-0e56-4cce-b83b-441374c30a32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b7cad42-1f19-4c77-901b-aaf719274950",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "196783e6-0e56-4cce-b83b-441374c30a32",
                    "LayerId": "9bde5830-0da2-46c9-abdb-a3e60230577c"
                }
            ]
        },
        {
            "id": "28e1a2be-483d-4560-a83f-8bb42fba6d72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "207a0dcb-f9bc-43b9-8205-5045c801e101",
            "compositeImage": {
                "id": "7d4d6d94-130a-4405-85eb-228868a1f195",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28e1a2be-483d-4560-a83f-8bb42fba6d72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd241fe0-9630-4fc4-95a4-fd9a0c6461fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28e1a2be-483d-4560-a83f-8bb42fba6d72",
                    "LayerId": "9bde5830-0da2-46c9-abdb-a3e60230577c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 19,
    "layers": [
        {
            "id": "9bde5830-0da2-46c9-abdb-a3e60230577c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "207a0dcb-f9bc-43b9-8205-5045c801e101",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 0,
    "yorig": 5
}
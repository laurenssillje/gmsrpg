{
    "id": "a537f90e-596c-40d2-a22b-2818ca3fd7bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lighting_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2f54794c-31cc-455c-8847-d7ac4f21d92e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a537f90e-596c-40d2-a22b-2818ca3fd7bf",
            "compositeImage": {
                "id": "65933ac0-c738-4eb8-a174-1fd55f996a13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f54794c-31cc-455c-8847-d7ac4f21d92e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bf37cb5-20f7-44f9-9038-42dc0864a486",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f54794c-31cc-455c-8847-d7ac4f21d92e",
                    "LayerId": "0819d6e3-7bba-4c13-a553-c396718b0b9e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0819d6e3-7bba-4c13-a553-c396718b0b9e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a537f90e-596c-40d2-a22b-2818ca3fd7bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
{
    "id": "4e2bf066-29e2-4b58-ac4d-5548ad41db44",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_transition_clock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 271,
    "bbox_left": 0,
    "bbox_right": 479,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b3bbba30-172c-4d05-a656-e3a9911fe58c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e2bf066-29e2-4b58-ac4d-5548ad41db44",
            "compositeImage": {
                "id": "cd3c6f33-9cdf-4572-a942-f6d6bb8e864e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3bbba30-172c-4d05-a656-e3a9911fe58c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63228105-038d-4bdd-b210-5618c639cd42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3bbba30-172c-4d05-a656-e3a9911fe58c",
                    "LayerId": "564f1233-eadd-4eb7-9768-7b894781cfa2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 272,
    "layers": [
        {
            "id": "564f1233-eadd-4eb7-9768-7b894781cfa2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e2bf066-29e2-4b58-ac4d-5548ad41db44",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 0,
    "yorig": 0
}
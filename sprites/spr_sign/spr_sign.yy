{
    "id": "34cfdf6c-20d4-4bc8-9f3c-722370c43757",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sign",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29e656ce-d0fc-4c70-8005-be3c6e22c9f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34cfdf6c-20d4-4bc8-9f3c-722370c43757",
            "compositeImage": {
                "id": "878ab2ec-59cf-46e4-855f-af3eb1a3a5fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29e656ce-d0fc-4c70-8005-be3c6e22c9f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f869cf6c-2740-4334-8028-16a8e7a93b08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29e656ce-d0fc-4c70-8005-be3c6e22c9f0",
                    "LayerId": "3bed937b-1759-435a-9ffd-0d386af25367"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3bed937b-1759-435a-9ffd-0d386af25367",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "34cfdf6c-20d4-4bc8-9f3c-722370c43757",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "b549f956-78fe-4918-9a2a-c206910df409",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_transition_fade",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 282,
    "bbox_left": 0,
    "bbox_right": 499,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c9e1086-c344-4506-b903-f0f94d1053a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b549f956-78fe-4918-9a2a-c206910df409",
            "compositeImage": {
                "id": "7259c55d-06ae-4fa3-a9f0-bc78ade2311a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c9e1086-c344-4506-b903-f0f94d1053a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8312df1-8219-4b05-813c-ff285b8ad789",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c9e1086-c344-4506-b903-f0f94d1053a4",
                    "LayerId": "ff62299f-0059-4e16-9d14-74df27e81a36"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 283,
    "layers": [
        {
            "id": "ff62299f-0059-4e16-9d14-74df27e81a36",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b549f956-78fe-4918-9a2a-c206910df409",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 500,
    "xorig": 0,
    "yorig": 0
}
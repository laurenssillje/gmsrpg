{
    "id": "5485cd35-47fc-4337-90b7-d6ad2d67d074",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_transition_triangle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 282,
    "bbox_left": 0,
    "bbox_right": 499,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "21c75d89-5126-4175-bc00-0ee1a44ac0aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5485cd35-47fc-4337-90b7-d6ad2d67d074",
            "compositeImage": {
                "id": "632e879a-339e-47f3-b957-39d4c461c440",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21c75d89-5126-4175-bc00-0ee1a44ac0aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e57df63-e33b-4c3e-9b36-5d191de5828a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21c75d89-5126-4175-bc00-0ee1a44ac0aa",
                    "LayerId": "4b1bc00e-48cc-4646-a390-2aa2fa18d795"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 283,
    "layers": [
        {
            "id": "4b1bc00e-48cc-4646-a390-2aa2fa18d795",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5485cd35-47fc-4337-90b7-d6ad2d67d074",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 500,
    "xorig": 0,
    "yorig": 0
}
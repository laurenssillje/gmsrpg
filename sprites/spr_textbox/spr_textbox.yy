{
    "id": "cd6e8a36-1f93-4577-a5df-f60b28081813",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_textbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 379,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3648f472-636e-4da3-b4c4-dd433c58cb90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd6e8a36-1f93-4577-a5df-f60b28081813",
            "compositeImage": {
                "id": "36c3278f-f741-49e5-8fcf-bc9a58141b0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3648f472-636e-4da3-b4c4-dd433c58cb90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "531573ad-ec09-4d5b-8221-f7bcb3eed678",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3648f472-636e-4da3-b4c4-dd433c58cb90",
                    "LayerId": "46854076-448a-4d5d-83eb-9938b4f104b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "46854076-448a-4d5d-83eb-9938b4f104b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd6e8a36-1f93-4577-a5df-f60b28081813",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 380,
    "xorig": 0,
    "yorig": 0
}
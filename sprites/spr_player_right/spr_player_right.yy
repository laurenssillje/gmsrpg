{
    "id": "c2978d8c-0be3-4a54-824a-d0b3640f4de9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c03cfec-8f0c-47c3-a49d-09744a9b9b6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2978d8c-0be3-4a54-824a-d0b3640f4de9",
            "compositeImage": {
                "id": "02ee4502-1967-4e26-bf25-0714767c96b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c03cfec-8f0c-47c3-a49d-09744a9b9b6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9cf4a8b-976a-490b-a4c1-705c20ad44e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c03cfec-8f0c-47c3-a49d-09744a9b9b6e",
                    "LayerId": "9381ffb4-41c6-4c69-a116-2edf49060871"
                }
            ]
        },
        {
            "id": "853b3209-bde8-46a9-b57e-215069dce77e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2978d8c-0be3-4a54-824a-d0b3640f4de9",
            "compositeImage": {
                "id": "fafcec7e-627c-43a1-82b1-b00ed740c72b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "853b3209-bde8-46a9-b57e-215069dce77e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edf38408-5e7a-4542-9901-e86fd7bcf596",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "853b3209-bde8-46a9-b57e-215069dce77e",
                    "LayerId": "9381ffb4-41c6-4c69-a116-2edf49060871"
                }
            ]
        },
        {
            "id": "05cfb1b8-95d6-4717-9ceb-b8ff109faab1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2978d8c-0be3-4a54-824a-d0b3640f4de9",
            "compositeImage": {
                "id": "78d06d53-1add-48a2-90dc-55ef57e7d6eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05cfb1b8-95d6-4717-9ceb-b8ff109faab1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ea3260f-5069-4efe-8e9e-ea45760be0c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05cfb1b8-95d6-4717-9ceb-b8ff109faab1",
                    "LayerId": "9381ffb4-41c6-4c69-a116-2edf49060871"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 19,
    "layers": [
        {
            "id": "9381ffb4-41c6-4c69-a116-2edf49060871",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c2978d8c-0be3-4a54-824a-d0b3640f4de9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 0,
    "yorig": 5
}
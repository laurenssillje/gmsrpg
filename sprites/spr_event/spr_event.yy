{
    "id": "9a6494d0-efee-4b48-a685-ebe826132682",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_event",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8dda0e4-55d2-4f60-b727-e43c85711c0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a6494d0-efee-4b48-a685-ebe826132682",
            "compositeImage": {
                "id": "c7ea8d44-acb2-4f91-bb1f-28c906604977",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8dda0e4-55d2-4f60-b727-e43c85711c0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "979df937-fc12-4af3-98e9-15d4e435eef4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8dda0e4-55d2-4f60-b727-e43c85711c0b",
                    "LayerId": "5bf2feff-08a2-490f-ac8f-d3f3c8cc31ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5bf2feff-08a2-490f-ac8f-d3f3c8cc31ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a6494d0-efee-4b48-a685-ebe826132682",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}